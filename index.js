
//require directive tells us to load the express module
const express = require('express'); 

const app = express(); //creating a server using express

const port = 4000; //port 


//middlewares - methods that express js use
app.use(express.json()) //allowa app to read a json data
app.use(express.urlencoded({extended: true})); 
	//allows appt to read data from forms
	//by default, information received from the url can only be received as string or an array
	//with extended: true - this allows to receive information in other data types such as objects.
// mock database

let users = [
	{
		email: "nezukokamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tanjirokamado@gmail.com",
		username: "gopanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	},
]

let loggedUser; 

app.get('/', (req, res) => {
	res.send('Hello World')
});

// app - server
//GET = http method
// '/' = route name or endpoint
// (req, res) - request and response = will handle requests and responses
// res.end - combined writeHead() and .end(), used to send response to our client



/*===mini activivty====*/

app.get('/hello', (req, res) => {
	res.send('Hello from Batch 131')
});


//post method
app.post('/', (req,res) => {
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}.`)
});

// use console.log to check what's inside the request body

app.post('/users/register', (req, res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser);
	console.log(users);

	res.send(`Users ${req.body.username} has successfully registered.`)
});

//login route

app.post('/users/login', (req, res) => {
	console.log(req.body);

	let foundUser = users.find((user) => {
		return user.username === req.body.username && user.password === req.body.password;
	});

	if (foundUser !== undefined) {

		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});

		foundUser.index = foundUserIndex;
		loggedUser = foundUser

		console.log(foundUser)
		res.send('Thank you for logging in.')

	} else {
		loggedUser = foundUser;
		res.send('Login Failed, wrong credentials.')
	}
})

//change password



app.put('/users/change-password' ,(req, res) => {
	let message;

	for(let i = 0; i < users.length; i++) {

		if (req.body.username === users[i].username) {

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been changed`; 
			break;
		} else {
			message = `User Not Found.`
		}
	}

	res.send(message)
})


//=== ACTIVITY =====

app.get('/home', (req, res) => {
	res.send("Hi!")
});


app.get('/users', (req, res) => {
	res.send(users)
});






app.listen(port, () => console.log(`The Server is running at port ${port}`));
	//listen to the port and returning messagee in the terminal
